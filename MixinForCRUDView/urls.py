
from django.contrib import admin
from django.urls import path
from testapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', views.EmployeeListModelMixin.as_view()),
    path('api/<int:pk>', views.EmployeeRetrieveUpdateDestroyModelMixin.as_view()),
]
