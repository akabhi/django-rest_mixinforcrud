from django.shortcuts import render
from testapp.serializers import EmployeeSerializer
from testapp.models import Employee
from rest_framework import mixins
from rest_framework import generics


# for lsit and create operation
class EmployeeListModelMixin(mixins.CreateModelMixin, generics.ListAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class EmployeeRetrieveUpdateDestroyModelMixin(mixins.UpdateModelMixin, mixins.DestroyModelMixin , generics.RetrieveAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)    
