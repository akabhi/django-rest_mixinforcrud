# Django-REST MixinForCRUD

All these operations are basically same as ListAPIView, RetrieveAPIView, RetrieveUpdateAPIView etc.
But internal implementaion of all these (ListAPIView, RetrieveAPIView, RetrieveUpdateAPIView etc.) classes are given by extending such mixins.

Better to use generics instead of mixins.
These mixins are just for understanding.

Even better to use ViewSet for simple CRUD operation.
